﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Флорист.EntityModel
{
    public partial class Product
    {
        public string getBg
        {
            get
            {
                if(ProductDiscountAmount > 15) return "#7fff00";
                return "#ffffff";
            }
        }

        public string CostStr
        {
            get
            {
                return ProductCost.ToString("0.00 руб\\.");
            }
        }

        public object IsDiscount
        {
            get
            {
                if (ProductDiscountAmount > 0)
                    return TextDecorations.Strikethrough;
                else
                    return null;
            }
        }

        public string DiscountStr
        {
            get
            {
                return ProductDiscountAmount.ToString("0.00\\%");
            }
        }

        public decimal FinalCost
        {
            get
            {
                return ProductCost * (1 - (decimal)ProductDiscountAmount / 100);
            }
        }

        public string FinalCostStr
        {
            get
            {
                if (ProductDiscountAmount > 0)
                    return FinalCost.ToString("0.00 руб\\.");
                else
                    return "";
            }
        }
    }
}
