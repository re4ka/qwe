﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Флорист.EntityModel;

namespace Флорист
{
    /// <summary>
    /// Логика взаимодействия для ProductList.xaml
    /// </summary>
    public partial class ProductList : Window
    {
        Order Order = new Order();

        public ProductList()
        {
            InitializeComponent();

            if (Authorization.IsAuthorized)
            {
                txtUser.Text = Authorization.User.FIO;
            }
            else
            {
                txtUser.Text = "Гость";
            }

            cbFilter.ItemsSource = new[]
            {
                new
                {
                    id = 1,
                    name = "Все диапазоны"
                },
                new
                {
                    id = 2,
                    name = "0% - 9,99%"
                },
                new
                {
                    id = 3,
                    name = "10% - 14,99%"
                },
                new
                {
                    id = 4,
                    name = "15% и более"
                },
            };
            cbFilter.SelectedValue = 1;

            cbSort.ItemsSource = new[]
            {
                new
                {
                    id = 1,
                    name = "Стоимость по возрастанию"
                },
                new
                {
                    id = 2,
                    name = "Стоимость по убыванию"
                },
            };
            cbSort.SelectedValue = 1;
            Load();
        }

        void Load()
        {
            string searchRequest = txtSearch.Text.ToLower();
            var products = Florist.Connection.Product.Where(
                p => p.ProductName.ToLower().Contains(searchRequest)
                || p.ProductDescription.ToLower().Contains(searchRequest)
                || p.ProductManufacturer.ToLower().Contains(searchRequest)
            );

            switch (cbFilter.SelectedValue)
            {
                case 2:
                    products = products.Where(p => p.ProductDiscountAmount < 10);
                    break;
                case 3:
                    products = products.Where(p => p.ProductDiscountAmount >= 10 && p.ProductDiscountAmount < 15);
                    break;
                case 4:
                    products = products.Where(p => p.ProductDiscountAmount >= 15);
                    break;
            }

            switch (cbSort.SelectedValue)
            {
                case 1:
                    products = products.OrderBy(p => p.ProductCost);
                    break;
                case 2:
                    products = products.OrderByDescending(p => p.ProductCost);
                    break;
            }

            icProducts.ItemsSource = products.ToList();

            txtCount.Text = $"Показано {products.Count()} из {Florist.Connection.Product.Count()}";
            
            if (Order.OrderProduct.Count > 0)
                btnOrder.Visibility = Visibility.Visible;
            else
                btnOrder.Visibility = Visibility.Hidden;
        }

        private void TxtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            Load();
        }

        private void CbFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Load();
        }

        private void CbSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Load();
        }

        private void BtnLogOut_Click(object sender, RoutedEventArgs e)
        {
            Authorization.LogOut();
            new AuthWindow().Show();
            Close();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Product p = (sender as MenuItem).DataContext as Product;
            if (Order.OrderProduct.FirstOrDefault(op => op.Product == p) == null)
            {
                Order.OrderProduct.Add(new OrderProduct() { Product = p, Amount = 1 });
            }
            Load();
        }

        private void BtnOrder_Click(object sender, RoutedEventArgs e)
        {
            new OrderWindow(Order).ShowDialog();
            if (Order.OrderStatus == "Новый") Order = new Order();
            Load();
        }
    }
}
