﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Флорист.EntityModel;

namespace Флорист
{
    /// <summary>
    /// Логика взаимодействия для OrderWindow.xaml
    /// </summary>
    public partial class OrderWindow : Window
    {
        Order Order;

        public OrderWindow(Order order)
        {
            InitializeComponent();

            Order = order;
            DataContext = Order;
            icOrderProducts.ItemsSource = Order.OrderProduct.ToList();

            if (Authorization.IsAuthorized)
                txtUser.Text = Authorization.User.FIO;
            else
                txtUser.Text = "Гость";
            cbPoint.ItemsSource = Florist.Connection.PickupPoints.ToList();

            Refresh();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("Вы точно хотите убрать товар из заказ?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Order.OrderProduct.Remove((sender as Button).DataContext as OrderProduct);
                icOrderProducts.ItemsSource = Order.OrderProduct.ToList();
                Refresh();
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            OrderProduct op = tb.DataContext as OrderProduct;
            if (op.Amount <= 0)
            {
                Order.OrderProduct.Remove(op);
                icOrderProducts.ItemsSource = Order.OrderProduct.ToList();
            }
            Refresh();
        }

        void Refresh()
        {
            txtSum.Text = $"Сумма заказа {Order.OrderProduct.Sum(op => op.Product.FinalCost * op.Amount).ToString("0.00")} руб.";
            btnOk.IsEnabled = Order.OrderProduct.Count > 0;
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(cbPoint.SelectedItem != null)
                {
                    Order.OrderStatus = "Новый";
                    Order.OrderCode = "123";
                    Order.OrderCreateDate = DateTime.Now;
                    Order.User = Authorization.User;
                    Florist.Connection.Order.Add(Order);
                    Florist.Connection.SaveChanges();
                    MessageBox.Show("Заказ оформлен");
                    Close();
                }
                else
                {
                    MessageBox.Show("Укажите пункт выдачи.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch
            {
                MessageBox.Show("Что-то пошло не так.\nОбратитесь к системному администратору.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
