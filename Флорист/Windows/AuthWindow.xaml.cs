﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Флорист.EntityModel;

namespace Флорист
{
    /// <summary>
    /// Логика взаимодействия для AuthWindow.xaml
    /// </summary>
    public partial class AuthWindow : Window
    {
        string captcha = "";

        public AuthWindow()
        {
            InitializeComponent();

            Florist.Connection.Product.ToList().ForEach(p =>
            {
                try
                {
                    p.ProductPhotoBitmap = File.ReadAllBytes(Directory.GetCurrentDirectory() + "/продукты_фото/" + p.ProductPhoto);
                }
                catch { }
            });

            Florist.Connection.SaveChanges();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (captcha == "")
            {
                if (Authorization.TryLogIn(txtLogin.Text, txtPass.Password))
                {
                    OpenProductList();
                }
                else
                {
                    MessageBox.Show("Неверный логин или пароль");

                    spCaptcha.Visibility = Visibility.Visible;

                    GenerateCaptcha();
                }
            }
            else
            {
                if (txtCaptcha.Text == captcha)
                {
                    if (Authorization.TryLogIn(txtLogin.Text, txtPass.Password))
                    {
                        OpenProductList();
                    }
                    else
                    {
                        Block();

                        MessageBox.Show("Неверный логин или пароль.\nПодождите 10 секунд.");
                    }
                }
                else
                {
                    Block();

                    MessageBox.Show("Капча введена неверно.\nПодождите 10 секунд.");
                }
            }
        }

        void Block()
        {
            IsEnabled = false;

            Task.Delay(new TimeSpan(0, 0, 10)).ContinueWith(t => Dispatcher.Invoke(() => {
                IsEnabled = true;
                GenerateCaptcha();
            }));
        }

        private void BtnGuest_Click(object sender, RoutedEventArgs e)
        {
            if(Authorization.IsAuthorized)
                Authorization.LogOut();

            OpenProductList();
        }

        void OpenProductList()
        {
            new ProductList().Show();

            Close();
        }

        private void CnvCaptcha_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            GenerateCaptcha();
        }

        void GenerateCaptcha()
        {
            string symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            Random r = new Random();

            cnvCaptcha.Children.Clear();
            txtCaptcha.Text = "";

            for (int i = 0; i < 4; i++)
            {
                int x = i * 50 + r.Next(5, 20);
                int y = r.Next(0, 18);
                string symbol = symbols.Substring(r.Next(0, symbols.Length), 1);
                captcha += symbol;
                cnvCaptcha.Children.Add(new TextBlock()
                {
                    Text = symbol,
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(x, y, 0, 0),
                    FontSize = 30,
                    Foreground = new SolidColorBrush(RandomColor(r))
                });
            }

            int lines = r.Next(2, 5);

            for (int i = 0; i < lines; i++)
            {
                cnvCaptcha.Children.Add(new Line()
                {
                    X1 = r.Next(5, 30),
                    X2 = r.Next(170, 195),
                    Y1 = r.Next(5, 55),
                    Y2 = r.Next(5, 55),
                    Stroke = new SolidColorBrush(RandomColor(r))
                });
            }
        }

        Color RandomColor(Random r)
        {
            return Color.FromRgb((byte)r.Next(0, 255), (byte)r.Next(0, 255), (byte)r.Next(0, 255));
        }
    }
}
