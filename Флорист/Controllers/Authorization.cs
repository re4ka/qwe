﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Флорист.EntityModel;

namespace Флорист
{
    static class Authorization
    {
        private static User user;

        public static User User
        {
            get
            {
                return user;
            }
            private set
            {
                user = value;
                IsAuthorized = user != null;
            }
        }

        public static bool IsAuthorized { get; private set; }

        public static bool TryLogIn(string login, string password)
        {
            User = Florist.Connection.User.FirstOrDefault(u => u.UserLogin == login && u.UserPassword == password);

            return IsAuthorized;
        }

        public static void LogOut()
        {
            User = null;
        }
    }
}